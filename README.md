# IRC a SRT

Subtitulado colaborativo en tiempo real, pensado para que un grupo de personas conectadas a una IRC traduzcan un documento en vivo y directo.

Pensando para subtitular a [RMS](https://es.wikipedia.org/wiki/Richard_Stallman) en el cumpleaños 30 de 
[GNU](https://es.wikipedia.org/wiki/GNU), subtitulado colaborativo vía [IRC](https://es.wikipedia.org/wiki/Irc) para subtitular un 
vídeo en vivo. Basado en la idea de [Active Archives](http://activearchives.org/wiki/Software#IRC2srt).

Además de traducir en vivo da la posibilidad de conservar el `.srt` para próximas reproducciones.

## ¿Como se usa?

![osd en el video](captura/osd.png)

~~~
mplayer video.ogg -input file=/tmp/irc2srt > /tmp/irc2mplayer
~~~
> Reproducir el vídeo con subtítulos colaborativos


![osd en el video](captura/irc.png)

~~~
irc2srt irc.hackcoop.com.ar 6697 6667 2> video.srt
~~~
> Iniciar el proxy de IRC conectar y unirse a un canal por ejemplo `#irc2srt`, todo lo que se digamos ahí va a ser publicado sobre el vídeo

## Lista que pendientes

* problemas con "\\", reemplazarla por "\\\\"
